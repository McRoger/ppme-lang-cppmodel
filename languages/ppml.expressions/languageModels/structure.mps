<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="q0gb" ref="r:4efcc790-8258-4a5c-a60f-10a5b5a367a2(de.ppme.base.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1082978499127" name="jetbrains.mps.lang.structure.structure.ConstrainedDataTypeDeclaration" flags="ng" index="Az7Fb">
        <property id="1083066089218" name="constraint" index="FLfZY" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="PlHQZ" id="5l83jlMf$L8">
    <property role="TrG5h" value="IAssignment" />
    <property role="3GE5qa" value="expr" />
  </node>
  <node concept="1TIwiD" id="5l83jlMf$Lb">
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="Expression" />
    <property role="3GE5qa" value="expr" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="PlHQZ" id="5l83jlMf$Lg">
    <property role="TrG5h" value="IBinary" />
    <property role="3GE5qa" value="expr" />
  </node>
  <node concept="1TIwiD" id="5l83jlMf$Lk">
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="BinaryExpression" />
    <property role="3GE5qa" value="expr" />
    <ref role="1TJDcQ" node="5l83jlMf$Lb" resolve="Expression" />
    <node concept="PrWs8" id="5l83jlMf$Ln" role="PzmwI">
      <ref role="PrY4T" node="5l83jlMf$Lg" resolve="IBinary" />
    </node>
    <node concept="1TJgyj" id="5l83jlMf$RD" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="left" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="5l83jlMf$RF" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="right" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMf$Lu">
    <property role="TrG5h" value="AssignmentExpression" />
    <property role="34LRSv" value="=" />
    <property role="R4oN_" value="assignment" />
    <property role="3GE5qa" value="expr" />
    <ref role="1TJDcQ" node="5l83jlMf$Lk" resolve="BinaryExpression" />
    <node concept="PrWs8" id="5l83jlMf$Lv" role="PzmwI">
      <ref role="PrY4T" node="5l83jlMf$L8" resolve="IAssignment" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMfE3M">
    <property role="TrG5h" value="UnaryExpression" />
    <property role="3GE5qa" value="expr" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="5l83jlMf$Lb" resolve="Expression" />
    <node concept="1TJgyj" id="5l83jlMfE3N" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="expression" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMfP2B">
    <property role="R5$K7" value="false" />
    <property role="TrG5h" value="Literal" />
    <property role="3GE5qa" value="literals" />
    <property role="R5$K2" value="true" />
    <ref role="1TJDcQ" node="5l83jlMf$Lb" resolve="Expression" />
  </node>
  <node concept="1TIwiD" id="5l83jlMfP4a">
    <property role="3GE5qa" value="literals.boolean" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="BooleanLiteral" />
    <property role="34LRSv" value="boolean constant" />
    <ref role="1TJDcQ" node="5l83jlMfP2B" resolve="Literal" />
    <node concept="1TJgyi" id="6IDtJdlkt6l" role="1TKVEl">
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMfP4s">
    <property role="3GE5qa" value="literals.boolean" />
    <property role="TrG5h" value="TrueLiteral" />
    <property role="34LRSv" value="true" />
    <ref role="1TJDcQ" node="5l83jlMfP4a" resolve="BooleanLiteral" />
  </node>
  <node concept="1TIwiD" id="5l83jlMfP4H">
    <property role="3GE5qa" value="literals.boolean" />
    <property role="TrG5h" value="FalseLiteral" />
    <property role="34LRSv" value="false" />
    <ref role="1TJDcQ" node="5l83jlMfP4a" resolve="BooleanLiteral" />
  </node>
  <node concept="Az7Fb" id="5l83jlMfThx">
    <property role="3GE5qa" value="literals.real" />
    <property role="TrG5h" value="FloatingNumberString" />
    <property role="FLfZY" value="(-?)(\\d+|\\d*.\\d+)" />
  </node>
  <node concept="1TIwiD" id="5l83jlMfWSw">
    <property role="TrG5h" value="Sheet" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5l83jlMfWT7" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="exps" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMfYoB">
    <property role="3GE5qa" value="literals.real" />
    <property role="TrG5h" value="DecimalLiteral" />
    <ref role="1TJDcQ" node="m1E9k9aYCP" resolve="RealLiteral" />
    <node concept="1TJgyi" id="5l83jlMfYoC" role="1TKVEl">
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" node="5l83jlMfThx" resolve="FloatingNumberString" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMgOLI">
    <property role="3GE5qa" value="expr.arith" />
    <property role="TrG5h" value="PlusExpression" />
    <property role="34LRSv" value="+" />
    <ref role="1TJDcQ" node="5l83jlMf$Lk" resolve="BinaryExpression" />
  </node>
  <node concept="1TIwiD" id="5l83jlMgOU3">
    <property role="3GE5qa" value="expr.arith" />
    <property role="TrG5h" value="MinusExpression" />
    <property role="34LRSv" value="-" />
    <ref role="1TJDcQ" node="5l83jlMf$Lk" resolve="BinaryExpression" />
  </node>
  <node concept="1TIwiD" id="5l83jlMgWog">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="ParenthesizedExpression" />
    <property role="34LRSv" value="(" />
    <property role="R4oN_" value="parenthesize" />
    <ref role="1TJDcQ" node="5l83jlMfE3M" resolve="UnaryExpression" />
  </node>
  <node concept="PlHQZ" id="5l83jlMhoVs">
    <property role="TrG5h" value="IVariableReference" />
    <property role="3GE5qa" value="variables" />
    <node concept="PrWs8" id="5l83jlMivno" role="PrDN$">
      <ref role="PrY4T" to="q0gb:5l83jlMivlg" resolve="IReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6fgLCPsAWoz">
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="Type" />
    <property role="3GE5qa" value="types" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6fgLCPsB20E" role="PzmwI">
      <ref role="PrY4T" to="tpck:hYa1RjM" resolve="IType" />
    </node>
  </node>
  <node concept="1TIwiD" id="6fgLCPsBwMC">
    <property role="3GE5qa" value="types" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="PrimitiveType" />
    <ref role="1TJDcQ" node="6fgLCPsAWoz" resolve="Type" />
  </node>
  <node concept="PlHQZ" id="6fgLCPsBxds">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="INumber" />
    <node concept="PrWs8" id="6fgLCPsBxf0" role="PrDN$">
      <ref role="PrY4T" node="6fgLCPsBxdX" resolve="INumeric" />
    </node>
    <node concept="PrWs8" id="7CEicFMLWXj" role="PrDN$">
      <ref role="PrY4T" node="7CEicFMLWOK" resolve="IOrdered" />
    </node>
  </node>
  <node concept="PlHQZ" id="6fgLCPsBxdX">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="INumeric" />
  </node>
  <node concept="1TIwiD" id="6fgLCPsBxeW">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="IntegerType" />
    <property role="34LRSv" value="integer" />
    <property role="R4oN_" value="integer type" />
    <ref role="1TJDcQ" node="6fgLCPsBwMC" resolve="PrimitiveType" />
    <node concept="PrWs8" id="7dQBydg08At" role="PzmwI">
      <ref role="PrY4T" node="6fgLCPsBxds" resolve="INumber" />
    </node>
  </node>
  <node concept="1TIwiD" id="6fgLCPsBxqM">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="RealType" />
    <property role="34LRSv" value="real" />
    <property role="R4oN_" value="real type" />
    <ref role="1TJDcQ" node="6fgLCPsBwMC" resolve="PrimitiveType" />
    <node concept="PrWs8" id="6fgLCPsBxqN" role="PzmwI">
      <ref role="PrY4T" node="6fgLCPsBxds" resolve="INumber" />
    </node>
  </node>
  <node concept="1TIwiD" id="6fgLCPsBxs4">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="BooleanType" />
    <property role="34LRSv" value="boolean" />
    <property role="R4oN_" value="logical type" />
    <ref role="1TJDcQ" node="6fgLCPsBwMC" resolve="PrimitiveType" />
    <node concept="PrWs8" id="dGdbRZjrNO" role="PzmwI">
      <ref role="PrY4T" node="dGdbRZjrKl" resolve="IComparable" />
    </node>
  </node>
  <node concept="PlHQZ" id="6fgLCPsByeK">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="ITyped" />
    <node concept="1TJgyj" id="6fgLCPsByeL" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="6fgLCPsAWoz" resolve="Type" />
    </node>
  </node>
  <node concept="1TIwiD" id="6fgLCPsD6lb">
    <property role="3GE5qa" value="literals" />
    <property role="TrG5h" value="StringLiteral" />
    <property role="34LRSv" value="&quot;" />
    <property role="R4oN_" value="string literal" />
    <ref role="1TJDcQ" node="5l83jlMfP2B" resolve="Literal" />
    <node concept="1TJgyi" id="6fgLCPsD6ll" role="1TKVEl">
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="PlHQZ" id="dGdbRZjrKl">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="IComparable" />
  </node>
  <node concept="1TIwiD" id="dGdbRZjYf0">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="StringType" />
    <property role="34LRSv" value="string" />
    <property role="R4oN_" value="character string type" />
    <ref role="1TJDcQ" node="6fgLCPsBwMC" resolve="PrimitiveType" />
  </node>
  <node concept="1TIwiD" id="2NzQxypWqRy">
    <property role="3GE5qa" value="expr.arith" />
    <property role="TrG5h" value="MulExpression" />
    <property role="34LRSv" value="*" />
    <ref role="1TJDcQ" node="5l83jlMf$Lk" resolve="BinaryExpression" />
  </node>
  <node concept="1TIwiD" id="2NzQxypWqRA">
    <property role="3GE5qa" value="expr.arith" />
    <property role="TrG5h" value="DivExpression" />
    <property role="34LRSv" value="/" />
    <ref role="1TJDcQ" node="5l83jlMf$Lk" resolve="BinaryExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMHLqj">
    <property role="3GE5qa" value="expr.logical" />
    <property role="TrG5h" value="AndExpression" />
    <property role="34LRSv" value="&amp;&amp;" />
    <property role="R4oN_" value="logical and" />
    <ref role="1TJDcQ" node="7CEicFMIxoS" resolve="BinaryLogicalExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMLYuw">
    <property role="3GE5qa" value="expr.compare" />
    <property role="TrG5h" value="LessEqualsExpressions" />
    <property role="R4oN_" value="less equals" />
    <property role="34LRSv" value="&lt;=" />
    <ref role="1TJDcQ" node="7CEicFMLVK3" resolve="OrderedComparisonExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMIxIn">
    <property role="3GE5qa" value="expr.logical" />
    <property role="TrG5h" value="UnaryLogicalExpression" />
    <property role="R4oN_" value="--" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="5l83jlMfE3M" resolve="UnaryExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMHWg2">
    <property role="3GE5qa" value="expr.logical" />
    <property role="TrG5h" value="OrExpression" />
    <property role="34LRSv" value="||" />
    <property role="R4oN_" value="logical or" />
    <ref role="1TJDcQ" node="7CEicFMIxoS" resolve="BinaryLogicalExpression" />
  </node>
  <node concept="PlHQZ" id="7CEicFMLWOK">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="IOrdered" />
  </node>
  <node concept="1TIwiD" id="7CEicFMIxoS">
    <property role="3GE5qa" value="expr.logical" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="BinaryLogicalExpression" />
    <property role="R4oN_" value="--" />
    <ref role="1TJDcQ" node="5l83jlMf$Lk" resolve="BinaryExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMLVK3">
    <property role="3GE5qa" value="expr.compare" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="OrderedComparisonExpression" />
    <property role="R4oN_" value="--" />
    <ref role="1TJDcQ" node="7CEicFMLTKj" resolve="ComparisonExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMLYsn">
    <property role="3GE5qa" value="expr.compare" />
    <property role="TrG5h" value="GreaterExpression" />
    <property role="R4oN_" value="greater than" />
    <property role="34LRSv" value="&gt;" />
    <ref role="1TJDcQ" node="7CEicFMLVK3" resolve="OrderedComparisonExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMLUvl">
    <property role="3GE5qa" value="expr.compare" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="EqualityComparisonExpression" />
    <property role="R4oN_" value="--" />
    <ref role="1TJDcQ" node="7CEicFMLTKj" resolve="ComparisonExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMLYrk">
    <property role="3GE5qa" value="expr.compare" />
    <property role="TrG5h" value="LessExpression" />
    <property role="R4oN_" value="less than" />
    <property role="34LRSv" value="&lt;" />
    <ref role="1TJDcQ" node="7CEicFMLVK3" resolve="OrderedComparisonExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMLTKj">
    <property role="3GE5qa" value="expr.compare" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="ComparisonExpression" />
    <property role="R4oN_" value="--" />
    <ref role="1TJDcQ" node="5l83jlMf$Lk" resolve="BinaryExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMLYqi">
    <property role="3GE5qa" value="expr.compare" />
    <property role="TrG5h" value="NotEqualsExpression" />
    <property role="34LRSv" value="!=" />
    <property role="R4oN_" value="not equals" />
    <ref role="1TJDcQ" node="7CEicFMLUvl" resolve="EqualityComparisonExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMLYph">
    <property role="3GE5qa" value="expr.compare" />
    <property role="TrG5h" value="EqualsExpression" />
    <property role="34LRSv" value="==" />
    <property role="R4oN_" value="equals" />
    <ref role="1TJDcQ" node="7CEicFMLUvl" resolve="EqualityComparisonExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMLYtr">
    <property role="3GE5qa" value="expr.compare" />
    <property role="TrG5h" value="GreaterEqualsExpression" />
    <property role="R4oN_" value="greater equals" />
    <property role="34LRSv" value="&gt;=" />
    <ref role="1TJDcQ" node="7CEicFMLVK3" resolve="OrderedComparisonExpression" />
  </node>
  <node concept="1TIwiD" id="7CEicFMHX3E">
    <property role="3GE5qa" value="expr.logical" />
    <property role="TrG5h" value="NotExpression" />
    <property role="34LRSv" value="!" />
    <property role="R4oN_" value="logical not" />
    <ref role="1TJDcQ" node="7CEicFMIxIn" resolve="UnaryLogicalExpression" />
  </node>
  <node concept="1TIwiD" id="3SvAy0XHRc7">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="MatrixType" />
    <property role="R5$K7" value="false" />
    <property role="34LRSv" value="matrix" />
    <ref role="1TJDcQ" node="3SvAy0XHWyX" resolve="AbstractContainerType" />
  </node>
  <node concept="1TIwiD" id="3SvAy0XHWnV">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="VectorType" />
    <property role="34LRSv" value="vector" />
    <ref role="1TJDcQ" node="3SvAy0XHWyX" resolve="AbstractContainerType" />
    <node concept="1TJgyj" id="5nlyqYp8A3k" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="ndim" />
      <ref role="20lvS9" node="5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="3SvAy0XHWyX">
    <property role="3GE5qa" value="types" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractContainerType" />
    <ref role="1TJDcQ" node="6fgLCPsAWoz" resolve="Type" />
    <node concept="1TJgyj" id="3SvAy0XHWz0" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="componentType" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="6fgLCPsAWoz" resolve="Type" />
    </node>
    <node concept="PrWs8" id="3SvAy0XMHmM" role="PzmwI">
      <ref role="PrY4T" node="3SvAy0XMGHn" resolve="IIterable" />
    </node>
  </node>
  <node concept="1TIwiD" id="3SvAy0XKTvE">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="VectorElementAccess" />
    <ref role="1TJDcQ" node="5l83jlMf$Lb" resolve="Expression" />
    <node concept="1TJgyj" id="3SvAy0XKTvF" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="vector" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="3SvAy0XKTvH" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="index" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
  <node concept="PlHQZ" id="3SvAy0XMGHn">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="IIterable" />
  </node>
  <node concept="1TIwiD" id="2dq8QBBlz5g">
    <property role="3GE5qa" value="literals.real" />
    <property role="TrG5h" value="ScientificNumberLiteral" />
    <ref role="1TJDcQ" node="m1E9k9aYCP" resolve="RealLiteral" />
    <node concept="1TJgyi" id="2dq8QBBlAhr" role="1TKVEl">
      <property role="TrG5h" value="prefix" />
      <ref role="AX2Wp" node="5l83jlMfThx" resolve="FloatingNumberString" />
    </node>
    <node concept="1TJgyi" id="2dq8QBBlAhu" role="1TKVEl">
      <property role="TrG5h" value="postfix" />
      <ref role="AX2Wp" node="2dq8QBBp0iN" resolve="SimpleNumberString" />
    </node>
  </node>
  <node concept="Az7Fb" id="2dq8QBBp0iN">
    <property role="3GE5qa" value="literals.real" />
    <property role="TrG5h" value="SimpleNumberString" />
    <property role="FLfZY" value="[\\+\\-]?(\\d+)" />
  </node>
  <node concept="1TIwiD" id="2dq8QBBpO8s">
    <property role="3GE5qa" value="expr.arith" />
    <property role="TrG5h" value="UnaryMinusExpression" />
    <property role="34LRSv" value="-" />
    <ref role="1TJDcQ" node="2dq8QBBpOnB" resolve="UnaryArithmeticExpression" />
  </node>
  <node concept="1TIwiD" id="2dq8QBBpOnB">
    <property role="3GE5qa" value="expr.arith" />
    <property role="TrG5h" value="UnaryArithmeticExpression" />
    <ref role="1TJDcQ" node="5l83jlMfE3M" resolve="UnaryExpression" />
  </node>
  <node concept="1TIwiD" id="m1E9k98BVA">
    <property role="3GE5qa" value="literals" />
    <property role="TrG5h" value="IntegerLiteral" />
    <property role="R4oN_" value="integer literal" />
    <property role="34LRSv" value="integer literal" />
    <ref role="1TJDcQ" node="5l83jlMfP2B" resolve="Literal" />
    <node concept="1TJgyi" id="m1E9k98YZm" role="1TKVEl">
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="m1E9k9aYCP">
    <property role="3GE5qa" value="literals.real" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="RealLiteral" />
    <ref role="1TJDcQ" node="5l83jlMfP2B" resolve="Literal" />
  </node>
  <node concept="1TIwiD" id="4RcFQZxmfQ7">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="AbstractVectorType" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="6fgLCPsAWoz" resolve="Type" />
  </node>
  <node concept="1TIwiD" id="4RcFQZxmfVn">
    <property role="3GE5qa" value="types" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractMatrixType" />
    <ref role="1TJDcQ" node="6fgLCPsAWoz" resolve="Type" />
  </node>
  <node concept="1TIwiD" id="7Sere_CgOA2">
    <property role="3GE5qa" value="literals" />
    <property role="TrG5h" value="VectorLiteral" />
    <property role="R4oN_" value="vector literal" />
    <ref role="1TJDcQ" node="5l83jlMfP2B" resolve="Literal" />
    <node concept="1TJgyj" id="7Sere_CgRih" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="values" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
</model>

