<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8d9fc76a-100f-4d44-8987-0a52314ba857(de.ppme.analysis.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="c9eo" ref="r:493484be-3d66-4fd8-b261-ed47cd8c6981(de.ppme.statements.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="6054523464627964745" name="jetbrains.mps.lang.structure.structure.AttributeInfo_AttributedConcept" flags="ng" index="trNpa">
        <reference id="6054523464627965081" name="concept" index="trN6q" />
      </concept>
      <concept id="2992811758677295509" name="jetbrains.mps.lang.structure.structure.AttributeInfo" flags="ng" index="M6xJ_">
        <property id="7588428831955550663" name="role" index="Hh88m" />
        <child id="7588428831947959310" name="attributed" index="EQaZv" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="aOl7efxWUh">
    <property role="TrG5h" value="HerbieAnnotation" />
    <ref role="1TJDcQ" to="tpck:2ULFgo8_XDk" resolve="NodeAttribute" />
    <node concept="1TJgyj" id="3Sm5iJJjDgy" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="inputError" />
      <ref role="20lvS9" to="pfd6:m1E9k9aYCP" resolve="RealLiteral" />
    </node>
    <node concept="1TJgyj" id="3Sm5iJJjDg_" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="outputError" />
      <ref role="20lvS9" to="pfd6:m1E9k9aYCP" resolve="RealLiteral" />
    </node>
    <node concept="M6xJ_" id="aOl7efy2Ms" role="lGtFl">
      <property role="Hh88m" value="herbie" />
      <node concept="trNpa" id="aOl7efyb2_" role="EQaZv">
        <ref role="trN6q" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      </node>
    </node>
    <node concept="1TJgyj" id="1P5nnDyLH4t" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="replacement" />
      <ref role="20lvS9" to="c9eo:5l83jlMhgCt" resolve="Statement" />
    </node>
  </node>
  <node concept="1TIwiD" id="pw96F8L5EY">
    <property role="TrG5h" value="RangeAnnotation" />
    <ref role="1TJDcQ" to="tpck:2ULFgo8_XDk" resolve="NodeAttribute" />
    <node concept="1TJgyj" id="pw96F8L7MK" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="upperBound" />
      <ref role="20lvS9" to="tpee:gbb6qgO" resolve="FloatingPointConstant" />
    </node>
    <node concept="1TJgyj" id="pw96F8L7MN" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="lowerBound" />
      <ref role="20lvS9" to="tpee:gbb6qgO" resolve="FloatingPointConstant" />
    </node>
    <node concept="M6xJ_" id="pw96F8RY0q" role="lGtFl">
      <property role="Hh88m" value="valueRange" />
      <node concept="trNpa" id="pw96F8RY0M" role="EQaZv">
        <ref role="trN6q" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      </node>
    </node>
  </node>
</model>

