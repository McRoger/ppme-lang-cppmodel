<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6aad060d-5c59-4427-a20d-74e3018ea29e(de.ppme.physunits.generator.template.main@generator)">
  <persistence version="9" />
  <languages>
    <use id="cd53cec3-9093-4895-940d-de1b7abe9936" name="de.ppme.physunits" version="-1" />
    <use id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core" version="-1" />
    <use id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext" version="-1" />
    <use id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="-1" />
    <generationPart ref="a206eff4-e667-4146-8006-8cce4ef80954(de.ppme.modules)" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpf8" ref="r:00000000-0000-4000-0000-011c895902e8(jetbrains.mps.lang.generator.structure)" />
    <import index="ote2" ref="r:505a4475-a865-4dba-9f1a-b7c0cce39ec5(de.ppme.physunits.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts">
      <concept id="1161622665029" name="jetbrains.mps.lang.sharedConcepts.structure.ConceptFunctionParameter_model" flags="nn" index="1Q6Npb" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1195502100749" name="preMappingScript" index="1puA0r" />
      </concept>
      <concept id="1195499912406" name="jetbrains.mps.lang.generator.structure.MappingScript" flags="lg" index="1pmfR0">
        <property id="1195595592106" name="scriptKind" index="1v3f2W" />
        <property id="1195595611951" name="modifiesModel" index="1v3jST" />
        <child id="1195501105008" name="codeBlock" index="1pqMTA" />
      </concept>
      <concept id="1195500722856" name="jetbrains.mps.lang.generator.structure.MappingScript_CodeBlock" flags="in" index="1pplIY" />
      <concept id="1195502151594" name="jetbrains.mps.lang.generator.structure.MappingScriptReference" flags="lg" index="1puMqW">
        <reference id="1195502167610" name="mappingScript" index="1puQsG" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1171323947159" name="jetbrains.mps.lang.smodel.structure.Model_NodesOperation" flags="nn" index="2SmgA7">
        <reference id="1171323947160" name="concept" index="2SmgA8" />
      </concept>
      <concept id="1144146199828" name="jetbrains.mps.lang.smodel.structure.Node_CopyOperation" flags="nn" index="1$rogu" />
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="298Hrzl$YVy">
    <property role="TrG5h" value="main" />
    <node concept="1puMqW" id="4XypWerhFFD" role="1puA0r">
      <ref role="1puQsG" node="4XypWerhAnZ" resolve="removeUnitAnnotations" />
    </node>
  </node>
  <node concept="1pmfR0" id="4XypWerhAnZ">
    <property role="TrG5h" value="removeUnitAnnotations" />
    <property role="1v3f2W" value="pre_processing" />
    <property role="1v3jST" value="true" />
    <node concept="1pplIY" id="4XypWerhAo0" role="1pqMTA">
      <node concept="3clFbS" id="4XypWerhAo1" role="2VODD2">
        <node concept="2Gpval" id="4XypWerhAo5" role="3cqZAp">
          <node concept="2GrKxI" id="4XypWerhAo6" role="2Gsz3X">
            <property role="TrG5h" value="annotatedType" />
          </node>
          <node concept="3clFbS" id="4XypWerhAo7" role="2LFqv$">
            <node concept="3clFbF" id="4XypWerhDY5" role="3cqZAp">
              <node concept="2OqwBi" id="4XypWerhDZR" role="3clFbG">
                <node concept="2GrUjf" id="4XypWerhDY4" role="2Oq$k0">
                  <ref role="2Gs0qQ" node="4XypWerhAo6" resolve="annotatedType" />
                </node>
                <node concept="1P9Npp" id="4XypWerhEpQ" role="2OqNvi">
                  <node concept="2OqwBi" id="4XypWeriETS" role="1P9ThW">
                    <node concept="2OqwBi" id="4XypWerhEsn" role="2Oq$k0">
                      <node concept="2GrUjf" id="4XypWerhEqe" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="4XypWerhAo6" resolve="annotatedType" />
                      </node>
                      <node concept="3TrEf2" id="4XypWerhEQv" role="2OqNvi">
                        <ref role="3Tt5mk" to="ote2:298HrzlF7GJ" />
                      </node>
                    </node>
                    <node concept="1$rogu" id="4XypWeriF3W" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="4XypWerhApT" role="2GsD0m">
            <node concept="1Q6Npb" id="4XypWerhAp9" role="2Oq$k0" />
            <node concept="2SmgA7" id="4XypWerhAtK" role="2OqNvi">
              <ref role="2SmgA8" to="ote2:298HrzlCobB" resolve="AnnotatedType" />
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="4XypWerhEVV" role="3cqZAp">
          <node concept="2GrKxI" id="4XypWerhEVW" role="2Gsz3X">
            <property role="TrG5h" value="annotatedExpr" />
          </node>
          <node concept="3clFbS" id="4XypWerhEVX" role="2LFqv$">
            <node concept="3clFbF" id="4XypWerhEVY" role="3cqZAp">
              <node concept="2OqwBi" id="4XypWerhEVZ" role="3clFbG">
                <node concept="2GrUjf" id="4XypWerhEW0" role="2Oq$k0">
                  <ref role="2Gs0qQ" node="4XypWerhEVW" resolve="annotatedExpr" />
                </node>
                <node concept="1P9Npp" id="4XypWerhEW1" role="2OqNvi">
                  <node concept="2OqwBi" id="4XypWeriFwb" role="1P9ThW">
                    <node concept="2OqwBi" id="4XypWerhFsl" role="2Oq$k0">
                      <node concept="2GrUjf" id="4XypWerhEW3" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="4XypWerhEVW" resolve="annotatedExpr" />
                      </node>
                      <node concept="3TrEf2" id="4XypWerhFCa" role="2OqNvi">
                        <ref role="3Tt5mk" to="ote2:298HrzlFgxM" />
                      </node>
                    </node>
                    <node concept="1$rogu" id="4XypWeriFC4" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="4XypWerhEW5" role="2GsD0m">
            <node concept="1Q6Npb" id="4XypWerhEW6" role="2Oq$k0" />
            <node concept="2SmgA7" id="4XypWerhEW7" role="2OqNvi">
              <ref role="2SmgA8" to="ote2:298HrzlDAuk" resolve="AnnotatedExpression" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

