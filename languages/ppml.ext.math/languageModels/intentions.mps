<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:79f47fbe-a36a-428d-87a7-ebb977ae2702(de.ppme.core.intentions)">
  <persistence version="9" />
  <languages>
    <use id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="2gyk" ref="r:be57a480-399b-470c-860e-bdfcf98e0b2c(de.ppme.core.structure)" />
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
    </language>
    <language id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions">
      <concept id="1192794744107" name="jetbrains.mps.lang.intentions.structure.IntentionDeclaration" flags="ig" index="2S6QgY" />
      <concept id="1192794782375" name="jetbrains.mps.lang.intentions.structure.DescriptionBlock" flags="in" index="2S6ZIM" />
      <concept id="1192795911897" name="jetbrains.mps.lang.intentions.structure.ExecuteBlock" flags="in" index="2Sbjvc" />
      <concept id="1192796902958" name="jetbrains.mps.lang.intentions.structure.ConceptFunctionParameter_node" flags="nn" index="2Sf5sV" />
      <concept id="2522969319638091381" name="jetbrains.mps.lang.intentions.structure.BaseIntentionDeclaration" flags="ig" index="2ZfUlf">
        <property id="2522969319638091386" name="isAvailableInChildNodes" index="2ZfUl0" />
        <reference id="2522969319638198290" name="forConcept" index="2ZfgGC" />
        <child id="2522969319638198291" name="executeFunction" index="2ZfgGD" />
        <child id="2522969319638093993" name="descriptionFunction" index="2ZfVej" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138757581985" name="jetbrains.mps.lang.smodel.structure.Link_SetNewChildOperation" flags="nn" index="zfrQC" />
      <concept id="6407023681583036853" name="jetbrains.mps.lang.smodel.structure.NodeAttributeQualifier" flags="ng" index="3CFYIy">
        <reference id="6407023681583036854" name="attributeConcept" index="3CFYIx" />
      </concept>
      <concept id="6407023681583031218" name="jetbrains.mps.lang.smodel.structure.AttributeAccess" flags="nn" index="3CFZ6_">
        <child id="6407023681583036852" name="qualifier" index="3CFYIz" />
      </concept>
      <concept id="1140133623887" name="jetbrains.mps.lang.smodel.structure.Node_DeleteOperation" flags="nn" index="1PgB_6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="2S6QgY" id="7bntxMfEwWd">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="toggleMK" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="pfd6:m1E9k9aYCP" resolve="RealLiteral" />
    <node concept="2Sbjvc" id="7bntxMfEwWe" role="2ZfgGD">
      <node concept="3clFbS" id="7bntxMfEwWf" role="2VODD2">
        <node concept="3clFbJ" id="7bntxMfIxzN" role="3cqZAp">
          <node concept="3clFbS" id="7bntxMfIxzO" role="3clFbx">
            <node concept="3clFbF" id="7bntxMfIxQ5" role="3cqZAp">
              <node concept="2OqwBi" id="7bntxMfIDik" role="3clFbG">
                <node concept="2OqwBi" id="7bntxMfID2Q" role="2Oq$k0">
                  <node concept="2Sf5sV" id="7bntxMfID0M" role="2Oq$k0" />
                  <node concept="3CFZ6_" id="7bntxMfIDen" role="2OqNvi">
                    <node concept="3CFYIy" id="7bntxMfIDfR" role="3CFYIz">
                      <ref role="3CFYIx" to="2gyk:7bntxMfBCN1" resolve="MyKindAnnotation" />
                    </node>
                  </node>
                </node>
                <node concept="1PgB_6" id="7bntxMfIDCZ" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="7bntxMfIxP6" role="3clFbw">
            <node concept="10Nm6u" id="7bntxMfIxPI" role="3uHU7w" />
            <node concept="2OqwBi" id="7bntxMfIxAn" role="3uHU7B">
              <node concept="2Sf5sV" id="7bntxMfIx$2" role="2Oq$k0" />
              <node concept="3CFZ6_" id="7bntxMfIxLm" role="2OqNvi">
                <node concept="3CFYIy" id="7bntxMfIxMj" role="3CFYIz">
                  <ref role="3CFYIx" to="2gyk:7bntxMfBCN1" resolve="MyKindAnnotation" />
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="7bntxMfIzgI" role="9aQIa">
            <node concept="3clFbS" id="7bntxMfIzgJ" role="9aQI4">
              <node concept="3clFbF" id="7bntxMfIzhG" role="3cqZAp">
                <node concept="2OqwBi" id="7bntxMfIzGd" role="3clFbG">
                  <node concept="2OqwBi" id="7bntxMfIzjz" role="2Oq$k0">
                    <node concept="2Sf5sV" id="7bntxMfIzhF" role="2Oq$k0" />
                    <node concept="3CFZ6_" id="7bntxMfIzD7" role="2OqNvi">
                      <node concept="3CFYIy" id="7bntxMfIzDK" role="3CFYIz">
                        <ref role="3CFYIx" to="2gyk:7bntxMfBCN1" resolve="MyKindAnnotation" />
                      </node>
                    </node>
                  </node>
                  <node concept="zfrQC" id="7bntxMfIzRo" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2S6ZIM" id="7bntxMfEwWg" role="2ZfVej">
      <node concept="3clFbS" id="7bntxMfEwWh" role="2VODD2">
        <node concept="3clFbF" id="7bntxMfEx8p" role="3cqZAp">
          <node concept="Xl_RD" id="7bntxMfEx8o" role="3clFbG">
            <property role="Xl_RC" value="toggle MK annotation" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

