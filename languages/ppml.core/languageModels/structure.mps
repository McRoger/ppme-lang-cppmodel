<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8a0fa9ef-3e8e-4313-a85b-46b3640418fd(de.ppme.modules.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="lm2c" ref="r:10261a70-d383-49ff-b567-775cda9fba27(de.ppme.ctrl.structure)" />
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="q0gb" ref="r:4efcc790-8258-4a5c-a60f-10a5b5a367a2(de.ppme.base.structure)" implicit="true" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="c9eo" ref="r:493484be-3d66-4fd8-b261-ed47cd8c6981(de.ppme.statements.structure)" implicit="true" />
    <import index="2gyk" ref="r:be57a480-399b-470c-860e-bdfcf98e0b2c(de.ppme.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="7eZWuAL6NR2">
    <property role="TrG5h" value="Module" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="module" />
    <property role="3GE5qa" value="module" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="7eZWuAL6NR3" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="5o9jvTM96Rx" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="PrWs8" id="7GORU49pSr0" role="PzmwI">
      <ref role="PrY4T" to="c9eo:7GORU49p0om" resolve="GlobalVariableDeclarationProvider" />
    </node>
    <node concept="1TJgyj" id="7eZWuAL6NSB" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="listOfImports" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="7eZWuAL6NSF" resolve="Import" />
    </node>
    <node concept="1TJgyj" id="7eZWuAL6RPP" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="listOfExternals" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="7eZWuAL6RNp" resolve="External" />
    </node>
    <node concept="1TJgyj" id="5l83jlMf9j5" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="listOfPhases" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="5l83jlMf16T" resolve="Phase" />
    </node>
    <node concept="1TJgyj" id="2NTMEjkMGlj" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="ctrlPropertySpecifications" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="lm2c:2NTMEjkMn3F" resolve="CtrlPropertySpecification" />
    </node>
    <node concept="1TJgyj" id="2NTMEjkT8ls" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="_addArgCalls" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2NTMEjkSL34" resolve="AddArgMacro" />
    </node>
    <node concept="1TJgyj" id="KVSbImREac" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="_rhs" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="KVSbImRE98" resolve="RightHandSideMacro" />
    </node>
    <node concept="1TJgyj" id="7aQhY_B1dtr" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="_ppmInit" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="tpck:gw2VY9q" resolve="BaseConcept" />
    </node>
    <node concept="1TJgyj" id="6r3vkxbWl_W" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="cfg" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="lm2c:5z7tqBB6nSj" resolve="Ctrl" />
    </node>
  </node>
  <node concept="1TIwiD" id="7eZWuAL6NSF">
    <property role="TrG5h" value="Import" />
    <property role="3GE5qa" value="module" />
    <property role="34LRSv" value="import" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7eZWuAL6RLq" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="library" />
      <ref role="20lvS9" node="7eZWuAL6RiF" resolve="Library" />
    </node>
    <node concept="1TJgyj" id="dGdbRZiBfa" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="shortAlias" />
      <ref role="20lvS9" node="dGdbRZiB83" resolve="AliasConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="7eZWuAL6RiF">
    <property role="TrG5h" value="Library" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="lib" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="7eZWuAL6RiG" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="7eZWuAL6RNp">
    <property role="3GE5qa" value="module" />
    <property role="TrG5h" value="External" />
    <property role="34LRSv" value="external" />
    <property role="R4oN_" value="required external parameters" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="6fgLCPsBUlc" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="link" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="c9eo:5l83jlMhBWg" resolve="VariableDeclaration" />
    </node>
    <node concept="1TJgyj" id="6fgLCPsD6B1" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="description" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" to="pfd6:6fgLCPsD6lb" resolve="StringLiteral" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMf16T">
    <property role="TrG5h" value="Phase" />
    <property role="34LRSv" value="phase" />
    <property role="3GE5qa" value="phase" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5o9jvTMbnTN" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="uses" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="3yJ4drhX9yy" resolve="PhaseInputDeclaration" />
    </node>
    <node concept="1TJgyj" id="5o9jvTMaq6I" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="provides" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="5o9jvTMaoHN" resolve="PhaseOutputDeclaration" />
    </node>
    <node concept="PrWs8" id="5l83jlMf6vk" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="5o9jvTM6I9P" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="PrWs8" id="7GORU49pWpN" role="PzmwI">
      <ref role="PrY4T" to="c9eo:7GORU49p0om" resolve="GlobalVariableDeclarationProvider" />
    </node>
    <node concept="1TJgyj" id="5l83jlMfKuN" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="body" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="c9eo:5l83jlMhgFI" resolve="StatementList" />
    </node>
  </node>
  <node concept="1TIwiD" id="dGdbRZiB83">
    <property role="TrG5h" value="AliasConcept" />
    <property role="3GE5qa" value="module" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="dGdbRZiB84" role="PzmwI">
      <ref role="PrY4T" to="q0gb:6fgLCPsE7bT" resolve="IIdentifierNamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="5o9jvTMaoHN">
    <property role="3GE5qa" value="phase" />
    <property role="TrG5h" value="PhaseOutputDeclaration" />
    <property role="34LRSv" value="out" />
    <ref role="1TJDcQ" to="c9eo:5l83jlMhBWg" resolve="VariableDeclaration" />
  </node>
  <node concept="1TIwiD" id="3yJ4drhX9yy">
    <property role="3GE5qa" value="phase" />
    <property role="TrG5h" value="PhaseInputDeclaration" />
    <property role="R4oN_" value="in &lt;var&gt;" />
    <property role="34LRSv" value="in" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5rrFAeHUiE8" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="outputReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5o9jvTMaoHN" resolve="PhaseOutputDeclaration" />
    </node>
    <node concept="1TJgyj" id="3z8Mov8atRo" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="propertyDeclaration" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="3z8Mov8aL2J" resolve="InputPropertyDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="3z8Mov8aL2J">
    <property role="3GE5qa" value="phase" />
    <property role="TrG5h" value="InputPropertyDeclaration" />
    <ref role="1TJDcQ" to="c9eo:3z8Mov8aLkc" resolve="LocalVariableDeclaration" />
  </node>
  <node concept="1TIwiD" id="2NTMEjkSL34">
    <property role="3GE5qa" value="ppml" />
    <property role="TrG5h" value="AddArgMacro" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2NTMEjkSEaf" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:6fgLCPsAWoz" resolve="Type" />
    </node>
    <node concept="1TJgyj" id="2NTMEjkSEak" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="default" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2NTMEjkSEan" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="min" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyi" id="2NTMEjkSEad" role="1TKVEl">
      <property role="TrG5h" value="name" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="2NTMEjkSEaA" role="1TKVEl">
      <property role="TrG5h" value="help_txt" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="2NTMEjkSEaK" role="1TKVEl">
      <property role="TrG5h" value="ctrl_name" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="2Xvn13HcKmh">
    <property role="3GE5qa" value="ppml" />
    <property role="TrG5h" value="CreatePropertyMacro" />
    <ref role="1TJDcQ" to="c9eo:5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="2Xvn13HcKQR" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="pset" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
    </node>
    <node concept="1TJgyj" id="2Xvn13HcKRD" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="ndim" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2Xvn13HcKSv" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="prec" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2Xvn13HcKSz" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="zero" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2Xvn13HcKSC" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="dtype" />
      <ref role="20lvS9" to="pfd6:6fgLCPsAWoz" resolve="Type" />
    </node>
    <node concept="PrWs8" id="2Xvn13HcKRM" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="2Xvn13HcLz4" role="1TKVEl">
      <property role="TrG5h" value="descr" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="m1E9k9iA_Y">
    <property role="3GE5qa" value="ppml" />
    <property role="TrG5h" value="PartLoopsMacro" />
    <ref role="1TJDcQ" to="c9eo:5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="m1E9k9iC9z" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="variable" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
    </node>
    <node concept="1TJgyj" id="m1E9k9iBeR" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pset" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="m1E9k9iAOS" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sca_field" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="m1E9k9iAOV" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="vec_field" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="m1E9k9iAPX" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sca_props" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="m1E9k9iAQ2" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="vec_props" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="m1E9k9k0GN" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="body" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="c9eo:5l83jlMhgFI" resolve="StatementList" />
    </node>
    <node concept="1TJgyi" id="m1E9k9iAP7" role="1TKVEl">
      <property role="TrG5h" value="writePos" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="4F3nn$ommkW">
    <property role="3GE5qa" value="ppml" />
    <property role="TrG5h" value="CreateFieldMacro" />
    <ref role="1TJDcQ" to="c9eo:5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="4F3nn$ommkX" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="pset" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
    </node>
    <node concept="1TJgyj" id="4F3nn$ommkY" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="ndim" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="4F3nn$omxTv" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="dtype" />
      <ref role="20lvS9" to="pfd6:6fgLCPsAWoz" resolve="Type" />
    </node>
    <node concept="PrWs8" id="4F3nn$omml2" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="4F3nn$omml3" role="1TKVEl">
      <property role="TrG5h" value="descr" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="1RtLc$XN06x">
    <property role="3GE5qa" value="ppml" />
    <property role="TrG5h" value="CreateODEMacro" />
    <ref role="1TJDcQ" to="c9eo:5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="1RtLc$XN08u" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="vars" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="1RtLc$XN08z" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="rhs_vars" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="1RtLc$XN08B" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="scheme" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyi" id="55TOEi0qcoy" role="1TKVEl">
      <property role="TrG5h" value="suffix" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="55TOEi0rsu2" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="rhs" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="KVSbImRE98" resolve="RightHandSideMacro" />
    </node>
  </node>
  <node concept="1TIwiD" id="KVSbImRE98">
    <property role="3GE5qa" value="ppml" />
    <property role="TrG5h" value="RightHandSideMacro" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="KVSbImSeEp" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="KVSbImSeHO" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="vars" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="KVSbImSeHT" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="plist" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="KVSbImSeFx" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="loop" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="m1E9k9iA_Y" resolve="PartLoopsMacro" />
    </node>
    <node concept="1TJgyj" id="KVSbImSxHP" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="diffops" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="2gyk:7yaypfC2_I8" resolve="DifferentialOperator" />
    </node>
    <node concept="1TJgyj" id="4Q17K_JItHg" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="differentials" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="2gyk:4Q17K_JsOfW" resolve="Differential" />
    </node>
    <node concept="1TJgyj" id="55TOEhZoACG" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="appliedOps" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="2bnyqnQb6p4">
    <property role="3GE5qa" value="ppml" />
    <property role="TrG5h" value="DefineOpMacro" />
    <ref role="1TJDcQ" to="c9eo:5l83jlMhBWg" resolve="VariableDeclaration" />
    <node concept="1TJgyj" id="2bnyqnQb6FY" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="degree" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2bnyqnQb6EE" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="coeffs" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyi" id="2bnyqnQb6IJ" role="1TKVEl">
      <property role="TrG5h" value="ndim" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="2bnyqnQbLtZ" role="1TKVEl">
      <property role="TrG5h" value="descr" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="52q84rKcqDg">
    <property role="3GE5qa" value="ppml" />
    <property role="TrG5h" value="DiscretizeOpMacro" />
    <ref role="1TJDcQ" to="c9eo:5l83jlMhBWg" resolve="VariableDeclaration" />
    <node concept="1TJgyj" id="52q84rKcqV0" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="num_method" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="52q84rKcqV4" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="method_params" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="52q84rKcrD7" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="operator" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
    </node>
    <node concept="1TJgyj" id="52q84rKcrDv" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="discretization" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="5j6OMFhZW1K">
    <property role="3GE5qa" value="ppml" />
    <property role="TrG5h" value="CheckOpDimsMacro" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="5j6OMFi06Ol" role="1TKVEl">
      <property role="TrG5h" value="opName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="5j6OMFi06On" role="1TKVEl">
      <property role="TrG5h" value="dimVar" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="5j6OMFi0daq" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="supportedDims" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="pfd6:m1E9k98BVA" resolve="IntegerLiteral" />
    </node>
  </node>
</model>

